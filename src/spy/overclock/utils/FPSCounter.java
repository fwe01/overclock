package spy.overclock.utils;

public class FPSCounter {
   public static int fps = 120;
   public static double timePerUpdate = 1000000000.0 / fps;
   public static double delta = 0;
   public static long now, lastTime = System.nanoTime();
   public static long timer = 0;
   public static int ticks = 0;

   public static void countTime(){
      now = System.nanoTime();
      delta += (now - lastTime) / timePerUpdate;
      timer += now - lastTime;
      lastTime = now;
   }

   public static void updateCounter(){
      ticks++;
      delta--;
   }

   public static void countFPS(){
      if (timer >= 1000000000){
         System.out.println("Ticks and Frames : " + ticks);
         ticks = 0;
         timer = 0;
      }
   }
}
