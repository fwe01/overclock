package spy.overclock.entity;

import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.handler.Handler;

import java.awt.*;

public abstract class Entity {
    public static final int DEFAULT_WIDTH = 128, DEFAULT_HEIGHT = 128;
    protected float x, y;
    protected int width, height;
    protected Handler handler;
    protected Rectangle collisionBox;
    private boolean enabled = true;

    public void translate(float deltaX, float deltaY) {
        this.x += deltaX;
        this.y += deltaY;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled() {
        this.enabled = true;
        this.initCollision();
    }

    public void setDisabled() {
        this.enabled = false;
        this.disableCollision();
    }

    public Entity(Handler handler, float x, float y, int width, int height) {
        this.handler = handler;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        collisionBox = new Rectangle(0, 0, width, height);
        initCollision();
    }

    public Entity(Handler handler, float x, float y) {
        this.handler = handler;
        this.x = x;
        this.y = y;
        this.width = DEFAULT_WIDTH;
        this.height = DEFAULT_HEIGHT;

        collisionBox = new Rectangle(0, 0, width, height);
        initCollision();
    }

    public Rectangle getCollisionBox(float xOffset, float yOffset) {
        return new Rectangle((int) (x + collisionBox.x + xOffset), (int) (y + collisionBox.y + yOffset), collisionBox.width, collisionBox.height);
    }

    public boolean checkEntityCollision(float xOffset, float yOffset) {
        for (Entity e : handler.getWorld().getEntityManager().getEntities()) {
            if (e.equals(this))
                continue;

            boolean intersects = e.getCollisionBox(0, 0).intersects(getCollisionBox(xOffset, yOffset));

            if (intersects) {
                if (e instanceof Pickupable) {
//                    ((Pickupable) e).setxMove(xOffset);
//                    ((Pickupable) e).setyMove(yOffset);
//                    ((Pickupable) e).move();
                    e.translate(xOffset, yOffset);
                }
                return true;
            }
        }
        return false;
    }

    protected void renderCollisionBox(Graphics graphics) {
        if (handler.getRenderCollisionBox()) {
            graphics.setColor(Color.red);
            graphics.drawRect((int) (x + collisionBox.x - handler.getCamera().getxOffset()),
                    (int) (y + collisionBox.y - handler.getCamera().getyOffset()), collisionBox.width, collisionBox.height);
            graphics.drawRect((int) (x + width / 2 - handler.getCamera().getxOffset()),
                    (int) (y + height / 2 - handler.getCamera().getyOffset()),
                    10,
                    10);
        }
    }

    public abstract void update();

    public abstract void render(Graphics graphics);

    protected abstract boolean collisionWithTile(int x, int y);

    protected abstract void initCollision();

    protected void disableCollision() {
        collisionBox.x = 0;
        collisionBox.y = 0;
        collisionBox.width = 0;
        collisionBox.height = 0;
    }

    public Handler getHandler() {
        return handler;
    }
}
