package spy.overclock.entity;

import spy.overclock.entity.controllable.player.Player;
import spy.overclock.handler.Handler;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;

public class EntityManager {
    private Handler handler;
    private Player player;
    private ArrayList<Entity> entities, unusedEntity, toBeAddedEntity;

    private Comparator<Entity> renderOrder = new Comparator<Entity>() {
        @Override
        public int compare(Entity e1, Entity e2) {
            if ((e1.getY() + e1.getHeight() / 2) > (e2.getY() + e2.getHeight() / 2)) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    public EntityManager(Handler handler, Player player) {
        this.handler = handler;
        this.player = player;
        entities = new ArrayList<Entity>();
        unusedEntity = new ArrayList<Entity>();
        toBeAddedEntity = new ArrayList<Entity>();
        addEntity(player);
    }

    public void addEntity(Entity e) {
        toBeAddedEntity.add(e);
    }

    public Entity getNewestAddedEntity(){
        return toBeAddedEntity.get(toBeAddedEntity.size() - 1);
    }

    public void update() {
        addNewEntity();
        for (Entity e : entities) {
            e.update();
        }
        entities.sort(renderOrder);
        deleteUnusedEntities();
    }

    private void addNewEntity() {
        entities.addAll(toBeAddedEntity);
        toBeAddedEntity.clear();
    }

    private void deleteUnusedEntities() {
        entities.removeAll(unusedEntity);
        unusedEntity.clear();
    }

    public void render(Graphics graphics) {
        for (Entity e : entities) {
            if (e.isEnabled()){
                e.render(graphics);
            }
        }
    }

    public void remove(Entity e){
        unusedEntity.add(e);
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }
}
