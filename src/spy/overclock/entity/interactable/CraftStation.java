package spy.overclock.entity.interactable;

import spy.overclock.handler.Handler;

public abstract class CraftStation extends Interactable {

    public CraftStation(Handler handler, float x, float y, int width, int height , float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public CraftStation(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public CraftStation(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public CraftStation(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    public abstract void craft();
}
