package spy.overclock.entity.interactable;

import spy.overclock.entity.Entity;
import spy.overclock.handler.Handler;

public abstract class Interactable extends Entity {
    public static final float DEFAULT_INTERACT_RANGE = 100f;
    protected float interactRange;

    public boolean isInRange(Entity parents) {
        float range = getRange(parents);
        if (range <= interactRange) {
            return true;
        } else
            return false;
    }

    public float getRange(Entity parents) {
        float parentX = parents.getX() + parents.getWidth() / 2;
        float parentY = parents.getY() + parents.getHeight() / 2;
        float centerX = x + width / 2;
        float centerY = y + height / 2;
        return (float) (Math.sqrt((Math.pow(parentX - centerX, 2) + Math.pow(parentY - centerY, 2))));
    }

    public Interactable(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height);
        this.interactRange = interactRange;
    }

    public Interactable(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        this.interactRange = DEFAULT_INTERACT_RANGE;
    }

    public Interactable(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y);
        this.interactRange = interactRange;
    }

    public Interactable(Handler handler, float x, float y) {
        super(handler, x, y);
        this.interactRange = DEFAULT_INTERACT_RANGE;
    }


}
