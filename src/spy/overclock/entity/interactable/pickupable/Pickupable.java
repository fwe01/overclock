package spy.overclock.entity.interactable.pickupable;

import spy.overclock.entity.Entity;
import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.Interactable;
import spy.overclock.handler.Handler;
import spy.overclock.utils.Direction;

public abstract class Pickupable extends Interactable {
    protected float pickupXOffset, pickupYOffset, xMove, yMove;
    protected boolean isPickedUp = false;
    protected Entity parents;

    public Pickupable(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
        initPickupOffset();
    }

    public Pickupable(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        initPickupOffset();
    }

    public Pickupable(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
        initPickupOffset();
    }

    public Pickupable(Handler handler, float x, float y) {
        super(handler, x, y);
        initPickupOffset();
    }

    @Override
    public void update() {
        if (isPickedUp && parents != null) {
            x = parents.getX() + pickupXOffset;
            y = parents.getY() + pickupYOffset;
        }
    }

    public void pickUp(Entity parents) {
        isPickedUp = true;
        this.parents = parents;
        disableCollision();
    }

    public boolean putDown() {
        isPickedUp = false;
        initCollision();
        switch (((Player) parents).getDirection().getCurrentDirection()) {
            case Direction.DOWN:
                y = parents.getY() + parents.getHeight() - collisionBox.y;
                break;
            case Direction.UP:
                y = parents.getY() + collisionBox.y;
                break;
            case Direction.LEFT:
                y = parents.getY() + collisionBox.y + (float) collisionBox.getHeight();
                x = parents.getX() - collisionBox.x;
                break;
            case Direction.RIGHT:
                y = parents.getY() + collisionBox.y + (float) collisionBox.getHeight();
                x = parents.getX() + parents.getWidth() - collisionBox.x;
                break;
        }
        parents = null;
        return true;
    }

    protected void initPickupOffset() {
        pickupXOffset = 20;
        pickupYOffset = -20;
    }

    public float getxMove() {
        return xMove;
    }

    public void setxMove(float xMove) {
        this.xMove = xMove;
    }

    public float getyMove() {
        return yMove;
    }

    public void setyMove(float yMove) {
        this.yMove = yMove;
    }

    public boolean isPickedUp() {
        return isPickedUp;
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return handler.getWorld().getTile(x, y).isSolid();
    }
}
