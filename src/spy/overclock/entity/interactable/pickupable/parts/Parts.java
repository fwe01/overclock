package spy.overclock.entity.interactable.pickupable.parts;

import spy.overclock.entity.Entity;
import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.entity.interactable.pickupable.resource.ResourceCounter;
import spy.overclock.handler.Handler;
import spy.overclock.utils.Direction;

public abstract class Parts extends Pickupable {
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT;

    public Parts(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Parts(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
    }

    public Parts(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
    }

    public Parts(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
    }

    @Override
    protected void initPickupOffset() {
        pickupXOffset = 7;
        pickupYOffset = -40;
    }

    @Override
    public boolean putDown() {
        isPickedUp = false;
        initCollision();
        switch (((Player) parents).getDirection().getCurrentDirection()) {
            case Direction.DOWN:
                y = parents.getY() + parents.getHeight() - collisionBox.y;
                break;
            case Direction.UP:
                y = parents.getY() - collisionBox.height;
                break;
            case Direction.LEFT:
                y = parents.getY() + collisionBox.y;
                x = parents.getX() - parents.getWidth() + collisionBox.x;
                break;
            case Direction.RIGHT:
                y = parents.getY() + collisionBox.y;
                x = parents.getX() + parents.getWidth() - collisionBox.x;
                break;
        }
        parents = null;
        return true;
    }
}
