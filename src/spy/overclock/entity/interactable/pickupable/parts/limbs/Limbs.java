package spy.overclock.entity.interactable.pickupable.parts.limbs;

import spy.overclock.entity.interactable.pickupable.parts.Parts;
import spy.overclock.handler.Handler;

public abstract class Limbs extends Parts {
    public Limbs(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Limbs(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public Limbs(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Limbs(Handler handler, float x, float y) {
        super(handler, x, y);
    }

}
