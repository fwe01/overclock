package spy.overclock.entity.interactable.pickupable.parts.head;

import spy.overclock.entity.interactable.pickupable.parts.Parts;
import spy.overclock.handler.Handler;

public abstract class Head extends Parts {
    public Head(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Head(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public Head(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Head(Handler handler, float x, float y) {
        super(handler, x, y);
    }
}
