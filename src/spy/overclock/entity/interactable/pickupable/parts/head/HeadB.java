package spy.overclock.entity.interactable.pickupable.parts.head;

import spy.overclock.entity.interactable.pickupable.resource.ResourceCounter;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class HeadB extends Head {
    public static final ResourceCounter RECIPE = new ResourceCounter(1, 2, 1);

    public HeadB(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public HeadB(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public HeadB(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public HeadB(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.partAssets.headB,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);

        if (handler.getRenderCollisionBox()) {
            graphics.setColor(Color.red);
            graphics.drawRect((int) (x + collisionBox.x - handler.getCamera().getxOffset()),
                    (int) (y + collisionBox.y - handler.getCamera().getyOffset()), collisionBox.width, collisionBox.height);
        }
    }

    @Override
    protected void initCollision() {

        collisionBox.x = 40;
        collisionBox.y = 45;
        collisionBox.width = 50;
        collisionBox.height = 30;
    }
}
