package spy.overclock.entity.interactable.pickupable.parts.torso;

import spy.overclock.entity.interactable.pickupable.resource.ResourceCounter;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class TorsoA extends Torso {
    public static ResourceCounter RECIPE = new ResourceCounter(2,1,3);

    public TorsoA(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public TorsoA(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public TorsoA(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public TorsoA(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.partAssets.torsoA,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);

        if (handler.getRenderCollisionBox()) {
            graphics.setColor(Color.red);
            graphics.drawRect((int) (x + collisionBox.x - handler.getCamera().getxOffset()),
                    (int) (y + collisionBox.y - handler.getCamera().getyOffset()), collisionBox.width, collisionBox.height);
        }
    }

    @Override
    protected void initCollision() {
        collisionBox.x = 40;
        collisionBox.y = 35;
        collisionBox.width = 50;
        collisionBox.height = 55;
    }
}
