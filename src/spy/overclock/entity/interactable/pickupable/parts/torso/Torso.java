package spy.overclock.entity.interactable.pickupable.parts.torso;

import spy.overclock.entity.interactable.pickupable.parts.Parts;
import spy.overclock.handler.Handler;

public abstract class Torso extends Parts {
    public Torso(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Torso(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Torso(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public Torso(Handler handler, float x, float y) {
        super(handler, x, y);
    }

}
