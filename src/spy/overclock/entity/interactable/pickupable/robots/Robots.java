package spy.overclock.entity.interactable.pickupable.robots;

import spy.overclock.entity.Entity;
import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.handler.Handler;
import spy.overclock.utils.Direction;

public abstract class Robots extends Pickupable {
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH * 3 / 2, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT * 3 / 2;

    public Robots(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Robots(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
    }

    public Robots(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
    }

    public Robots(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
    }

    @Override
    protected void initPickupOffset() {
        pickupXOffset = -20;
        pickupYOffset = -80;
    }

    @Override
    public boolean putDown() {
        isPickedUp = false;
        initCollision();
        switch (((Player) parents).getDirection().getCurrentDirection()) {
            case Direction.DOWN:
                y = parents.getY() + parents.getHeight() - collisionBox.y;
                break;
            case Direction.UP:
                y = parents.getY() - collisionBox.height;
                break;
            case Direction.LEFT:
                y = parents.getY() + collisionBox.y;
                x = parents.getX() - parents.getWidth() ;
                break;
            case Direction.RIGHT:
                y = parents.getY() + collisionBox.y;
                x = parents.getX() + parents.getWidth() - collisionBox.x;
                break;
        }
        parents = null;
        return true;
    }
}
