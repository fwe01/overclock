package spy.overclock.entity.interactable.pickupable.robots;

import spy.overclock.entity.Entity;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class RobotA extends Robots {
    private static final double DEFAULT_WIDTH = Entity.DEFAULT_WIDTH * 1.5, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT * 1.5;

    public RobotA(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public RobotA(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public RobotA(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public RobotA(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.robotAssets.robotA,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);

        if (handler.getRenderCollisionBox()) {
            graphics.setColor(Color.red);
            graphics.drawRect((int) (x + collisionBox.x - handler.getCamera().getxOffset()),
                    (int) (y + collisionBox.y - handler.getCamera().getyOffset()), collisionBox.width, collisionBox.height);
        }
    }

    @Override
    protected void initCollision() {
        collisionBox.x = 70;
        collisionBox.y = 45;
        collisionBox.width = 50;
        collisionBox.height = 110;
    }
}
