package spy.overclock.entity.interactable.pickupable.resource;

import java.util.Objects;

public class ResourceCounter {
    private int boltCount, platesCount, mechatronicsCount;

    public ResourceCounter(int boltCount, int platesCount, int mechatronicsCount) {
        this.boltCount = boltCount;
        this.platesCount = platesCount;
        this.mechatronicsCount = mechatronicsCount;
    }

    public ResourceCounter() {
        resetCount();
    }

    public int getBoltCount() {
        return boltCount;
    }

    public void incrementBoltCount() {
        this.boltCount++;
    }

    public int getPlatesCount() {
        return platesCount;
    }

    public void incrementPlatesCount() {
        this.platesCount++;
    }

    public int getMechatronicsCount() {
        return mechatronicsCount;
    }

    public void incrementMechatronicsCount() {
        this.mechatronicsCount++;
    }

    public void resetCount (){
        boltCount = 0;
        platesCount = 0;
        mechatronicsCount = 0;
    }


    public boolean isEmpty(){
        return (boltCount == 0 && platesCount == 0 && mechatronicsCount == 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceCounter that = (ResourceCounter) o;
        return boltCount == that.boltCount && platesCount == that.platesCount && mechatronicsCount == that.mechatronicsCount;
    }
}
