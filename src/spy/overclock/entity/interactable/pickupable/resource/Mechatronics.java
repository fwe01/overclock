package spy.overclock.entity.interactable.pickupable.resource;

import spy.overclock.entity.Entity;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class Mechatronics extends Resource{
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH / 2, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT / 2;
    public Mechatronics(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Mechatronics(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Mechatronics(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y,DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
    }

    public Mechatronics(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.resourceAssets.mechatronics,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        renderCollisionBox(graphics);
    }

    @Override
    protected void initCollision() {

        collisionBox.x = 15;
        collisionBox.y = 15;
        collisionBox.width = 35;
        collisionBox.height = 40;
    }
}
