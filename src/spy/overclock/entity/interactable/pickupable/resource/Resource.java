package spy.overclock.entity.interactable.pickupable.resource;

import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.handler.Handler;
import spy.overclock.utils.Direction;

public abstract class Resource extends Pickupable {
    public Resource(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public Resource(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Resource(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    public Resource(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    @Override
    public boolean putDown() {
        isPickedUp = false;
        initCollision();
        switch (((Player) parents).getDirection().getCurrentDirection()) {
            case Direction.DOWN:
                y = parents.getY() + parents.getHeight() - collisionBox.y;
                break;
            case Direction.UP:
                y = parents.getY() + collisionBox.y;
                break;
            case Direction.LEFT:
                y = parents.getY() + collisionBox.y + (float) collisionBox.getHeight();
                x = parents.getX() - parents.getWidth() + width;
                break;
            case Direction.RIGHT:
                y = parents.getY() + collisionBox.y + (float) collisionBox.getHeight();
                x = parents.getX() + parents.getWidth() - collisionBox.x;
                break;
        }
        parents = null;
        return true;
    }
}
