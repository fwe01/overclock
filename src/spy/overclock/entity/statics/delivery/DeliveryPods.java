package spy.overclock.entity.statics.delivery;

import spy.overclock.entity.Entity;
import spy.overclock.entity.EntityManager;
import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.Interactable;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.entity.interactable.pickupable.robots.RobotA;
import spy.overclock.entity.interactable.pickupable.robots.RobotB;
import spy.overclock.entity.interactable.pickupable.robots.Robots;
import spy.overclock.events.score.ScoreSubtracted.ScoreSubtracted;
import spy.overclock.graphics.Animation.Animation;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;
import spy.overclock.events.score.ScoreAdded.ScoreAdded;
import spy.overclock.events.score.ScoreEventListener;

import java.awt.*;
import java.util.ArrayList;

public class DeliveryPods extends Interactable {
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH * 3, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT * 3;
    private static final float DEFAULT_INTERACT_RANGE = 300;
    private static final int PODS_COOLDOWN = 5000;
    private Animation open_capsule_anim, send_capsule_anim;

    public DeliveryPods(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
        initAnim();
    }

    public DeliveryPods(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
        initAnim();
    }

    public DeliveryPods(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
        initAnim();
    }

    public DeliveryPods(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
        initAnim();
    }

    private void initAnim() {
        open_capsule_anim = new Animation(200, Assets.deliveryAssets.doorOpen);
        send_capsule_anim = new Animation(
                PODS_COOLDOWN / Assets.deliveryAssets.complete_send.length,
                Assets.deliveryAssets.complete_send
        );
        send_capsule_anim.setIndex(38);
    }

    @Override
    public void update() {
        EntityManager entityManager = handler.getWorld().getEntityManager();
        ArrayList<Entity> entities = entityManager.getEntities();
        for (Entity e : entities) {
            if (e instanceof Robots && e.isEnabled() && ((Pickupable) e).isInRange(this) && !((Pickupable) e).isPickedUp()) {
                if (e instanceof RobotA) {
                    ScoreEventListener.newScoreEvent(new ScoreAdded(500));
                } else if (e instanceof RobotB) {
                    ScoreEventListener.newScoreEvent(new ScoreAdded(400));
                } else {
                    ScoreEventListener.newScoreEvent(new ScoreSubtracted(250));
                }
                send_capsule_anim.setIndex(0);
                entityManager.remove(e);
            } else if (e instanceof Player) {
                if (this.isInRange(e)) {
                    if (open_capsule_anim.getIndex() < 5)
                        open_capsule_anim.update();
                } else {
                    open_capsule_anim.setIndex(0);
                }
            }
        }

        if (send_capsule_anim.getIndex() < 38)
            send_capsule_anim.update();
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(
//                Assets.deliveryAssets.doorOpen[0],
                send_capsule_anim.getIndex() < 38 ?
                        send_capsule_anim.getCurrentFrame() :
                        open_capsule_anim.getCurrentFrame(),
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        renderCollisionBox(graphics);
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return false;
    }

    @Override
    protected void initCollision() {
        collisionBox.x = 45;
        collisionBox.y = 160;
        collisionBox.width = 300;
        collisionBox.height = 220;
    }
}
