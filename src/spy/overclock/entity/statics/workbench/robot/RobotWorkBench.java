package spy.overclock.entity.statics.workbench.robot;

import spy.overclock.entity.Entity;
import spy.overclock.entity.EntityManager;
import spy.overclock.entity.interactable.CraftStation;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.entity.interactable.pickupable.parts.Parts;
import spy.overclock.entity.interactable.pickupable.parts.head.*;
import spy.overclock.entity.interactable.pickupable.parts.limbs.*;
import spy.overclock.entity.interactable.pickupable.parts.torso.*;
import spy.overclock.entity.interactable.pickupable.robots.*;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;
import java.util.ArrayList;

public class RobotWorkBench extends CraftStation {
    private static final int MAX_PART_COUNT = 3;
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH * 3, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT * 3;
    private static final float DEFAULT_INTERACT_RANGE = 300;
    private Head storedHead = null;
    private Torso storedTorso = null;
    private Limbs storedLimbs = null;

    public RobotWorkBench(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public RobotWorkBench(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
    }

    public RobotWorkBench(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
    }

    public RobotWorkBench(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
    }

    @Override
    public void update() {
        EntityManager entityManager = handler.getWorld().getEntityManager();
        ArrayList<Entity> entities = entityManager.getEntities();
        for (Entity e : entities) {
            if (e instanceof Parts && e.isEnabled() && ((Pickupable) e).isInRange(this) && !((Pickupable) e).isPickedUp()) {
                if (e instanceof Head && this.storedHead == null) {
                    this.storedHead = (Head) e;
                } else if (e instanceof Torso && this.storedTorso == null) {
                    this.storedTorso = (Torso) e;
                } else if (e instanceof Limbs && this.storedLimbs == null) {
                    this.storedLimbs = (Limbs) e;
                }
                entityManager.remove(e);
            }
        }
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.workbenchAssets.workbench_robot,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);

        renderCollisionBox(graphics);
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return false;
    }

    @Override
    protected void initCollision() {
        collisionBox.x = 60;
        collisionBox.y = 65;
        collisionBox.height = height - 128;
        collisionBox.width = width - 128;
    }

    @Override
    public void craft() {
        checkRobotRecipe();
        storedHead = null;
        storedTorso = null;
        storedLimbs = null;
    }

    private void checkRobotRecipe() {
        if (storedHead != null && storedTorso != null && storedLimbs != null) {
            int offsetX = 100,
                    offsetY = 240;
            if (storedHead instanceof HeadA && storedTorso instanceof TorsoA && storedLimbs instanceof LimbsA) {
                System.out.println("udah ke spawn");
                handler.getWorld().getEntityManager().addEntity(new RobotA(handler, x + offsetX, y + offsetY));
            } else if (storedHead instanceof HeadB && storedTorso instanceof TorsoB && storedLimbs instanceof LimbsB) {
                handler.getWorld().getEntityManager().addEntity(new RobotB(handler, x + offsetX, y + offsetY));
            } else {
                handler.getWorld().getEntityManager().addEntity(new RobotB(handler, x + offsetX, y + offsetY));
            }
        }
    }
}
