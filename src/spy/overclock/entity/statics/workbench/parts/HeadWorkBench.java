package spy.overclock.entity.statics.workbench.parts;

import spy.overclock.entity.interactable.pickupable.parts.BrokenPartB;
import spy.overclock.entity.interactable.pickupable.parts.head.HeadA;
import spy.overclock.entity.interactable.pickupable.parts.head.HeadB;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class HeadWorkBench extends PartWorkBench {
    public HeadWorkBench(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public HeadWorkBench(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    public HeadWorkBench(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public HeadWorkBench(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.workbenchAssets.workbench_head,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        renderCollisionBox(graphics);
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return false;
    }

    @Override
    protected void initCollision() {

    }

    @Override
    public void craft() {
        if (!resourceCounter.isEmpty()) {

            int offsetX = 0,
                    offsetY = 150;
            if (resourceCounter.equals(HeadA.RECIPE)) {
                handler.getWorld().getEntityManager().addEntity(new HeadA(handler, x + offsetX, y + offsetY));
            } else if (resourceCounter.equals(HeadB.RECIPE)) {
                handler.getWorld().getEntityManager().addEntity(new HeadB(handler, x + offsetX, y + offsetY));
            } else {
                handler.getWorld().getEntityManager().addEntity(new BrokenPartB(handler, x + offsetX, y + offsetY));
            }
        }
        resourceCounter.resetCount();
    }
}
