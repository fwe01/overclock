package spy.overclock.entity.statics.workbench.parts;

import spy.overclock.entity.interactable.pickupable.parts.BrokenPartA;
import spy.overclock.entity.interactable.pickupable.parts.limbs.LimbsA;
import spy.overclock.entity.interactable.pickupable.parts.limbs.LimbsB;
import spy.overclock.entity.interactable.pickupable.resource.ResourceCounter;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class LimbWorkBench extends PartWorkBench {
    public static final ResourceCounter RECIPE = new ResourceCounter(1, 0, 0);

    public LimbWorkBench(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
    }

    public LimbWorkBench(Handler handler, float x, float y) {
        super(handler, x, y);
    }

    public LimbWorkBench(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public LimbWorkBench(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.workbenchAssets.workbench_limbs_hands,
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        renderCollisionBox(graphics);
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return false;
    }

    @Override
    protected void initCollision() {

    }

    @Override
    public void craft() {
        if (!resourceCounter.isEmpty()) {
            int offsetX = 0,
                    offsetY = 150;
            if (resourceCounter.equals(LimbsA.RECIPE)) {
                handler.getWorld().getEntityManager().addEntity(new LimbsA(handler, x + offsetX, y + offsetY));
            } else if (resourceCounter.equals(LimbsB.RECIPE)) {
                handler.getWorld().getEntityManager().addEntity(new LimbsB(handler, x + offsetX, y + offsetY));
            } else {
                handler.getWorld().getEntityManager().addEntity(new BrokenPartA(handler, x + offsetX, y + offsetY));
            }
        }
        resourceCounter.resetCount();
    }
}
