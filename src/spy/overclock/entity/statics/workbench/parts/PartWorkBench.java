package spy.overclock.entity.statics.workbench.parts;

import spy.overclock.entity.Entity;
import spy.overclock.entity.EntityManager;
import spy.overclock.entity.interactable.CraftStation;
import spy.overclock.entity.interactable.pickupable.resource.*;
import spy.overclock.handler.Handler;

import java.util.ArrayList;

public abstract class PartWorkBench extends CraftStation {
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT;
    private static final float DEFAULT_INTERACT_RANGE = 300;

    protected ResourceCounter resourceCounter;

    public PartWorkBench(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
        resourceCounter = new ResourceCounter();
    }

    public PartWorkBench(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
        initCollision();
        resourceCounter = new ResourceCounter();
    }

    public PartWorkBench(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
        resourceCounter = new ResourceCounter();
    }

    public PartWorkBench(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
        resourceCounter = new ResourceCounter();
    }

    @Override
    public void update() {
        EntityManager entityManager = handler.getWorld().getEntityManager();
        ArrayList<Entity> entities = entityManager.getEntities();
        for (Entity e : entities) {
            if (e instanceof Resource) {
                if (e.isEnabled() && !((Resource) e).isPickedUp() && ((Resource) e).isInRange(this)) {
                    if (e instanceof Bolt) {
                        resourceCounter.incrementBoltCount();
                    } else if (e instanceof Plates) {
                        resourceCounter.incrementPlatesCount();
                    } else if (e instanceof Mechatronics) {
                        resourceCounter.incrementMechatronicsCount();
                    }
                    entityManager.remove(e);
                }
            }
        }
    }

    public abstract void craft();
}
