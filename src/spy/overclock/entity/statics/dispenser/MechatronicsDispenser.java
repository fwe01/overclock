package spy.overclock.entity.statics.dispenser;

import spy.overclock.entity.interactable.pickupable.resource.Mechatronics;
import spy.overclock.graphics.Animation.Animation;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class MechatronicsDispenser extends ResourceDispenser {
    public MechatronicsDispenser(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
        initAnim();
    }

    public MechatronicsDispenser(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        initAnim();
    }

    public MechatronicsDispenser(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, interactRange);
        initAnim();
    }

    public MechatronicsDispenser(Handler handler, float x, float y) {
        super(handler, x, y);
        initAnim();
    }

    private void initAnim() {
        this.animation = new Animation(
                DEFAULT_COOLDOWN / Assets.dispensersAssets.incomingMechatronics.length,
                Assets.dispensersAssets.incomingMechatronics);
    }

    @Override
    public void update() {
        if (!isReadyToPickup()) {
            if (animation.getIndex() < Assets.dispensersAssets.incomingMechatronics.length - 1)
                this.animation.update();
        }
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(
                isReadyToPickup() ?
                        Assets.dispensersAssets.incomingMechatronics[Assets.dispensersAssets.incomingMechatronics.length - 1] :
                        animation.getCurrentFrame(),
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        renderCollisionBox(graphics);
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return false;
    }

    @Override
    protected void initCollision() {

        collisionBox.x = 20;
        collisionBox.y = 40;
        collisionBox.width = 90;
        collisionBox.height = 80;
    }

    @Override
    public void dispense() {
        checkCooldown();
        if (readyToPickup) {
            handler.getWorld().getEntityManager().addEntity(new Mechatronics(handler, x, y + Y_DISPENSE_OFFSET));
            animation.setIndex(0);
        }
    }
}
