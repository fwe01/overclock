package spy.overclock.entity.statics.dispenser;

import spy.overclock.entity.Entity;
import spy.overclock.entity.interactable.Interactable;
import spy.overclock.graphics.Animation.Animation;
import spy.overclock.handler.Handler;

public abstract class ResourceDispenser extends Interactable {
    private static final int DEFAULT_WIDTH = Entity.DEFAULT_WIDTH, DEFAULT_HEIGHT = Entity.DEFAULT_HEIGHT;
    private static final float DEFAULT_INTERACT_RANGE = 300f;
    protected static final int Y_DISPENSE_OFFSET = 100;
    protected static final int DEFAULT_COOLDOWN = 2000;
    protected long lastTime, timer;
    protected boolean readyToPickup = true;
    protected Animation animation;

    public ResourceDispenser(Handler handler, float x, float y, int width, int height, float interactRange) {
        super(handler, x, y, width, height, interactRange);
        initTime();
    }

    public ResourceDispenser(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height, DEFAULT_INTERACT_RANGE);
        initTime();
    }

    public ResourceDispenser(Handler handler, float x, float y, float interactRange) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, interactRange);
        initTime();
    }

    public ResourceDispenser(Handler handler, float x, float y) {
        super(handler, x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_INTERACT_RANGE);
        initTime();
    }

    private void initTime() {
        lastTime = System.currentTimeMillis();
        timer = 0;
    }

    public abstract void dispense();

    protected void checkCooldown() {
        long now = System.currentTimeMillis();

        if (now - lastTime >= DEFAULT_COOLDOWN) {
            System.out.println("lol");

            lastTime = now;
            readyToPickup = true;
            return;
        }
        readyToPickup = false;
    }

    public boolean isReadyToPickup() {
        return readyToPickup;
    }

    public void setReadyToPickup(boolean readyToPickup) {
        this.readyToPickup = readyToPickup;
    }
}
