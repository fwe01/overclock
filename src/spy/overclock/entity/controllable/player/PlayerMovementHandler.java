package spy.overclock.entity.controllable.player;

import spy.overclock.handler.Handler;
import spy.overclock.utils.Direction;
import spy.overclock.utils.Vector2D;

public class PlayerMovementHandler {
    private Player player;

    public PlayerMovementHandler(Player player) {
        this.player = player;
    }

    public Vector2D checkMovement() {
        float speed = this.player.getSpeed();
        Handler handler = this.player.getHandler();

        boolean isUp, isDown, isRight, isLeft;
        isRight = handler.getKeyManager().right;
        isDown = handler.getKeyManager().down;
        isLeft = handler.getKeyManager().left;
        isUp = handler.getKeyManager().up;

        float xMove = 0;
        float yMove = 0;

        if (isUp && isDown) {
            isUp = isDown = false;
        }
        if (isRight && isLeft) {
            isRight = isLeft = false;
        }

        if (isUp && isRight) {
            yMove += -0.7071 * speed;
            xMove += 0.7071 * speed;
        } else if (isUp && isLeft) {
            yMove += -0.7071 * speed;
            xMove += -0.7071 * speed;
        } else if (isDown && isLeft) {
            yMove += 0.7071 * speed;
            xMove += -0.7071 * speed;
        } else if (isDown && isRight) {
            yMove += 0.7071 * speed;
            xMove += 0.7071 * speed;
        } else if (isUp)
            yMove += -1 * speed;
        else if (isDown)
            yMove += 1 * speed;
        else if (isLeft)
            xMove += -1 * speed;
        else if (isRight)
            xMove += 1 * speed;

        if (xMove < 0) player.getDirection().setCurrentDirection(Direction.LEFT);
        else if (xMove > 0) player.getDirection().setCurrentDirection(Direction.RIGHT);
        else if (yMove < 0) player.getDirection().setCurrentDirection(Direction.UP);
        else if (yMove > 0) player.getDirection().setCurrentDirection(Direction.DOWN);

        return new Vector2D(xMove, yMove);
    }
}
