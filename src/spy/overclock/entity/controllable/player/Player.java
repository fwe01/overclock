package spy.overclock.entity.controllable.player;

import spy.overclock.entity.Entity;
import spy.overclock.entity.controllable.Controllable;
import spy.overclock.entity.interactable.CraftStation;
import spy.overclock.entity.interactable.Interactable;
import spy.overclock.entity.statics.dispenser.ResourceDispenser;
import spy.overclock.entity.interactable.pickupable.Pickupable;
import spy.overclock.handler.Handler;
import spy.overclock.events.score.ScoreAdded.ScoreAdded;
import spy.overclock.events.score.ScoreEventListener;
import spy.overclock.utils.Direction;
import spy.overclock.utils.Vector2D;

import java.awt.*;

public class Player extends Controllable {
    public static final int DEFAULT_PLAYER_WIDTH = Entity.DEFAULT_WIDTH,
            DEFAULT_PLAYER_HEIGHT = Entity.DEFAULT_HEIGHT;

    private boolean
            pickingUp = false,
            interactWasPressed = false,
            crafting = false,
            craftWasPressed = false;

    private Pickupable pickedUpItem;
    private PlayerAnimationHandler playerAnimationHandler;
    private PlayerMovementHandler playerMovementHandler;

    private Direction direction;

    public Player(Handler handler, float x, float y, int width, int height, float speed) {
        super(handler, x, y, width, height, speed);
    }

    public Player(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

    public Player(Handler handler, float x, float y) {
        super(handler, x, y);
        this.width = DEFAULT_PLAYER_WIDTH;
        this.height = DEFAULT_PLAYER_HEIGHT;

        playerAnimationHandler = new PlayerAnimationHandler(this);
        playerMovementHandler = new PlayerMovementHandler(this);

        direction = new Direction(Direction.DOWN);
    }

    @Override
    protected void initCollision() {
        collisionBox.x = 38;
        collisionBox.y = 82;
        collisionBox.width = 52;
        collisionBox.height = 34;
    }

    @Override
    public void update() {
        playerAnimationHandler.update();
        getInput();
        move();
        handler.getCamera().centerOnEntity(this);
    }

    @Override
    public void getInput() {
        checkInteract();
        checkMovement();
        checkCraft();
    }

    private void checkCraft() {
        boolean isCraft = handler.getKeyManager().craft;
        if (isCraft && !craftWasPressed) {
            if (!crafting) {
                for (Entity e : handler.getWorld().getEntityManager().getEntities()) {
                    if (e instanceof CraftStation) {
                        if (((CraftStation) e).isInRange(this)) {
                            ((CraftStation) e).craft();
                            crafting = true;
                            break;
                        }
                    }
                }
            }
        }

        if (isCraft) {
            craftWasPressed = true;
        } else {
            craftWasPressed = false;
            crafting = false;
        }
    }

    private void checkInteract() {
        boolean isInteract = handler.getKeyManager().interact;
        Interactable nearestObject = null;
        float nearestDistance = Float.MAX_VALUE;
        if (isInteract && !interactWasPressed) {
            if (!pickingUp) {
                for (Entity e : handler.getWorld().getEntityManager().getEntities()) {
                    if (e instanceof Interactable) {
                        if (e.isEnabled() && ((Interactable) e).isInRange(this)) {
                            if (((Interactable) e).getRange(this) < nearestDistance) {
                                nearestDistance = Float.min(nearestDistance, ((Interactable) e).getRange(this));
                                nearestObject = (Interactable) e;
                            }
                        }
                    }
                }
                if (nearestObject != null) {
                    if (nearestObject instanceof Pickupable) {
                        pickedUpItem = (Pickupable) nearestObject;
                        pickedUpItem.pickUp(this);
                        pickingUp = true;
                    } else if (nearestObject instanceof ResourceDispenser) {
                        ((ResourceDispenser) nearestObject).dispense();
                        if (((ResourceDispenser) nearestObject).isReadyToPickup()) {
                            ((ResourceDispenser) nearestObject).setReadyToPickup(false);
                            pickedUpItem = (Pickupable) handler.getWorld().getEntityManager().getNewestAddedEntity();
                            pickedUpItem.pickUp(this);
                            pickingUp = true;

                        }
                    }
                }
            } else {
                if (pickedUpItem.putDown()) {
                    pickingUp = false;
                    pickedUpItem = null;
                }
            }
        }

        if (isInteract) {
            interactWasPressed = true;
        } else {
            interactWasPressed = false;
        }
    }

    private void checkMovement() {
        Vector2D movement = playerMovementHandler.checkMovement();
        xMove = movement.getX();
        yMove = movement.getY();
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(
                playerAnimationHandler.getCurrentAnimation(),
                (int) (x - handler.getCamera().getxOffset()),
                (int) (y - handler.getCamera().getyOffset()),
                width, height, null);
        //render collision box
        if (handler.getRenderCollisionBox()) {
            graphics.setColor(Color.red);
            graphics.drawRect(
                    (int) (x + collisionBox.x - handler.getCamera().getxOffset()),
                    (int) (y + collisionBox.y - handler.getCamera().getyOffset()),
                    collisionBox.width, collisionBox.height
            );
        }
    }

    @Override
    protected boolean collisionWithTile(int x, int y) {
        return handler.getWorld().getTile(x, y).isSolid();
    }

    public boolean isPickingUp() {
        return pickingUp;
    }

    public Direction getDirection() {
        return direction;
    }
}
