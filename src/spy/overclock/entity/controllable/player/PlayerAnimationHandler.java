package spy.overclock.entity.controllable.player;

import spy.overclock.graphics.Animation.Animation;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.utils.Direction;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class PlayerAnimationHandler {
    private ArrayList<Animation> player_animations;
    //walking anim
    private Animation
            animDown, animIdleDown,
            animUp, animIdleUp,
            animLeft, animIdleLeft,
            animRight, animIdleRight;

    //pickup anim
    private Animation
            animPickupDown, animPickupIdleDown,
            animPickupUp, animPickupIdleUp,
            animPickupLeft, animPickupIdleLeft,
            animPickupRight, animPickupIdleRight;

    private Player player;

    public PlayerAnimationHandler(Player player) {
        player_animations = new ArrayList<>();
        initAnimation();
        this.player = player;
    }

    private void initAnimation() {
        //walking anim
        player_animations.add(animRight = new Animation(250, Assets.playerAssets.player_right));
        player_animations.add(animLeft = new Animation(250, Assets.playerAssets.player_left));
        player_animations.add(animDown = new Animation(250, Assets.playerAssets.player_down));
        player_animations.add(animUp = new Animation(250, Assets.playerAssets.player_up));

        player_animations.add(animIdleRight = new Animation(250, Assets.playerAssets.player_idle_right));
        player_animations.add(animIdleLeft = new Animation(250, Assets.playerAssets.player_idle_left));
        player_animations.add(animIdleDown = new Animation(250, Assets.playerAssets.player_idle_down));
        player_animations.add(animIdleUp = new Animation(250, Assets.playerAssets.player_idle_up));

        //pickup anim
        player_animations.add(animPickupRight = new Animation(250, Assets.playerAssets.player_pickup_right));
        player_animations.add(animPickupLeft = new Animation(250, Assets.playerAssets.player_pickup_left));
        player_animations.add(animPickupDown = new Animation(250, Assets.playerAssets.player_pickup_down));
        player_animations.add(animPickupUp = new Animation(250, Assets.playerAssets.player_pickup_up));

        player_animations.add(animPickupIdleRight = new Animation(250, Assets.playerAssets.player_pickup_idle_right));
        player_animations.add(animPickupIdleLeft = new Animation(250, Assets.playerAssets.player_pickup_idle_left));
        player_animations.add(animPickupIdleDown = new Animation(250, Assets.playerAssets.player_pickup_idle_down));
        player_animations.add(animPickupIdleUp = new Animation(250, Assets.playerAssets.player_pickup_idle_up));
    }

    public void update() {
        for (Animation a : player_animations) {
            a.update();
        }
    }

    public BufferedImage getCurrentAnimation() {

        if (!player.isPickingUp())
            return getCurrentMoveAnimation(player.getXMove(), player.getYMove());
        else
            return getCurrentPickupAnimation(player.getXMove(), player.getYMove());
    }

    private BufferedImage getCurrentMoveAnimation(float xMove, float yMove) {
        if (playerIsIdle(xMove, yMove)) {
            switch (player.getDirection().getCurrentDirection()) {
                case Direction.DOWN:
                    return animIdleDown.getCurrentFrame();
                case Direction.RIGHT:
                    return animIdleRight.getCurrentFrame();
                case Direction.LEFT:
                    return animIdleLeft.getCurrentFrame();
                case Direction.UP:
                    return animIdleUp.getCurrentFrame();
            }
        }
        //move anim
        if (xMove < -1)
            return animLeft.getCurrentFrame();
        else if (xMove > 1)
            return animRight.getCurrentFrame();
        else if (yMove > 1)
            return animDown.getCurrentFrame();
        else if (yMove < -1)
            return animUp.getCurrentFrame();
        else
            return animIdleDown.getCurrentFrame();
    }

    private BufferedImage getCurrentPickupAnimation(float xMove, float yMove) {
        if (playerIsIdle(xMove, yMove)) {
            switch (player.getDirection().getCurrentDirection()) {
                case Direction.DOWN:
                    return animPickupIdleDown.getCurrentFrame();
                case Direction.RIGHT:
                    return animPickupIdleRight.getCurrentFrame();
                case Direction.LEFT:
                    return animPickupIdleLeft.getCurrentFrame();
                case Direction.UP:
                    return animPickupIdleUp.getCurrentFrame();
            }
        }

        if (xMove < -1)
            return animPickupLeft.getCurrentFrame();
        else if (xMove > 1)
            return animPickupRight.getCurrentFrame();
        else if (yMove > 1)
            return animPickupDown.getCurrentFrame();
        else if (yMove < -1)
            return animPickupUp.getCurrentFrame();
        else
            return animPickupIdleDown.getCurrentFrame();
    }


    private boolean playerIsIdle(float xMove, float yMove) {
        return xMove == 0 && yMove == 0;
    }

}
