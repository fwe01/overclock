package spy.overclock.entity.controllable;

import spy.overclock.handler.Handler;
import spy.overclock.entity.Entity;
import spy.overclock.tiles.Tile;

public abstract class Controllable extends Entity {
    public static final float DEFAULT_SPEED = 2.5f;
    protected float speed;
    protected float xMove, yMove;

    public Controllable(Handler handler, float x, float y, int width, int height, float speed) {
        super(handler, x, y, width, height);
        this.speed = speed;
    }

    public Controllable(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        this.speed = DEFAULT_SPEED;
    }

    public Controllable(Handler handler, float x, float y, float speed) {
        super(handler, x, y);
        this.speed = speed;
    }

    public Controllable(Handler handler, float x, float y) {
        super(handler, x, y);
        this.speed = DEFAULT_SPEED;
    }

    public void move() {
        if (!checkEntityCollision(xMove, 0))
            moveX();
        if (!checkEntityCollision(0, yMove))
            moveY();
    }

    public void moveX() {
        int tempTopY = (int) (y + collisionBox.y) / Tile.TILE_HEIGHT;
        int tempBotY = (int) (y + collisionBox.y + collisionBox.height) / Tile.TILE_HEIGHT;

        if (xMove > 0) {
            int tempX = (int) (x + xMove + collisionBox.x + collisionBox.width) / Tile.TILE_WIDTH;
            if (xMove != 0 && !collisionWithTile(tempX, tempTopY) && !collisionWithTile(tempX, tempBotY)) {
                x += xMove;
            } else {
                x = tempX * Tile.TILE_WIDTH - collisionBox.width - collisionBox.x - 1;
            }
        } else if (xMove < 0) {
            int tempX = (int) (x + xMove + collisionBox.x) / Tile.TILE_WIDTH;
            if (xMove != 0 && !collisionWithTile(tempX, tempTopY) && !collisionWithTile(tempX, tempBotY)) {
                x += xMove;
            } else {
                x = tempX * Tile.TILE_WIDTH + Tile.TILE_WIDTH - collisionBox.x;
            }
        }
    }

    public void moveY() {
        int tempLeftX = (int) (x + collisionBox.x) / Tile.TILE_WIDTH;
        int tempRightX = (int) (x + collisionBox.x + collisionBox.width) / Tile.TILE_WIDTH;
        if (yMove > 0) {
            int tempY = (int) (y + yMove + collisionBox.y + collisionBox.height) / Tile.TILE_HEIGHT;
            if (yMove != 0 && !collisionWithTile(tempLeftX, tempY) && !collisionWithTile(tempRightX, tempY)) {
                y += yMove;
            } else {
                y = tempY * Tile.TILE_HEIGHT - collisionBox.y - collisionBox.height - 1;
            }
        } else if (yMove < 0) {
            int tempY = (int) (y + yMove + collisionBox.y) / Tile.TILE_HEIGHT;
            if (yMove != 0 && !collisionWithTile(tempLeftX, tempY) && !collisionWithTile(tempRightX, tempY)) {
                y += yMove;
            } else {
                y = tempY * Tile.TILE_HEIGHT + Tile.TILE_HEIGHT - collisionBox.y;
            }
        }
    }

    public abstract void getInput();

    public float getXMove() {
        return xMove;
    }

    public void setXMove(float xMove) {
        this.xMove = xMove;
    }

    public float getYMove() {
        return yMove;
    }

    public void setYMove(float yMove) {
        this.yMove = yMove;
    }

    public float getSpeed() {
        return speed;
    }
}
