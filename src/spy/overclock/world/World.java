package spy.overclock.world;

import spy.overclock.entity.EntityManager;
import spy.overclock.entity.controllable.player.Player;
import spy.overclock.entity.interactable.pickupable.parts.head.HeadA;
import spy.overclock.entity.interactable.pickupable.parts.head.HeadB;
import spy.overclock.entity.interactable.pickupable.parts.limbs.LimbsA;
import spy.overclock.entity.interactable.pickupable.parts.limbs.LimbsB;
import spy.overclock.entity.interactable.pickupable.parts.torso.TorsoA;
import spy.overclock.entity.interactable.pickupable.parts.torso.TorsoB;
import spy.overclock.entity.interactable.pickupable.resource.Bolt;
import spy.overclock.entity.interactable.pickupable.resource.Mechatronics;
import spy.overclock.entity.interactable.pickupable.resource.Plates;
import spy.overclock.entity.interactable.pickupable.robots.RobotA;
import spy.overclock.entity.interactable.pickupable.robots.RobotB;
import spy.overclock.entity.statics.delivery.DeliveryPods;
import spy.overclock.entity.statics.dispenser.BoltDispenser;
import spy.overclock.entity.statics.dispenser.MechatronicsDispenser;
import spy.overclock.entity.statics.dispenser.PlateDispenser;
import spy.overclock.entity.statics.workbench.parts.HeadWorkBench;
import spy.overclock.entity.statics.workbench.parts.LimbWorkBench;
import spy.overclock.entity.statics.workbench.robot.RobotWorkBench;
import spy.overclock.entity.statics.workbench.parts.TorsoWorkBench;
import spy.overclock.graphics.Assets.ImageLoader;
import spy.overclock.graphics.Assets.WallAssets;
import spy.overclock.handler.Handler;
import spy.overclock.tiles.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;

public class World {

    private static final Color WALL_COLOR_ID = new Color(0, 0, 0, 255);
    private static final Color FLOOR_COLOR_ID = new Color(255, 255, 255, 255);
    private static final Color PLAYER_COLOR_ID = new Color(250, 240, 50, 255);

    private static final Color TORSOWB_COLOR_ID = new Color(255, 0, 0, 255);
    private static final Color HEADWB_COLOR_ID = new Color(255, 49, 49, 255);
    private static final Color LIMBSWB_COLOR_ID = new Color(255, 102, 102, 255);

    private static final Color ROBOTWB_COLOR_ID = new Color(0, 0, 255, 255);
    private static final Color DELIVERY_PODS_COLOR_ID = new Color(0, 0, 132, 255);

    private static final Color BOLT_DISPENSER_COLOR_ID = new Color(17, 255, 0, 255);
    private static final Color PLATE_DISPENSER_COLOR_ID = new Color(32, 211, 0, 255);
    private static final Color MECHATRONICS_DISPENSER_COLOR_ID = new Color(15, 101, 0, 255);

    public static final Color WORLD_BACKGROUND = new Color(5, 5, 60, 255);

    private Handler handler;
    private int width;
    private int height;
    private int playerSpawnX;
    private int playerSpawnY;
    private int themeCode;
    private int[][] worldTiles;
    private EntityManager entityManager;

    public World(Handler handler, String path, int themeCode) {
        this.handler = handler;
        this.themeCode = themeCode;
        entityManager = new EntityManager(handler, new Player(handler, 0, 0));
        loadWorld(path);
        entityManager.getPlayer().setX(playerSpawnX * Tile.TILE_WIDTH);
        entityManager.getPlayer().setY(playerSpawnY * Tile.TILE_HEIGHT);
    }

    public void update() {
        entityManager.update();
    }

    public void render(Graphics graphics) {
        int xStart = (int) Math.max(0, handler.getCamera().getxOffset() / Tile.TILE_WIDTH);
        int yStart = (int) Math.max(0, handler.getCamera().getyOffset() / Tile.TILE_HEIGHT);
        int xEnd = (int) Math.min(width, (handler.getCamera().getxOffset() + handler.getWidth()) / Tile.TILE_WIDTH + 1);
        int yEnd = (int) Math.min(height, (handler.getCamera().getyOffset() + handler.getHeight()) / Tile.TILE_HEIGHT + 1);

        for (int tileY = yStart; tileY < yEnd; tileY++) {
            for (int tileX = xStart; tileX < xEnd; tileX++) {
                this.getTile(tileX, tileY).render(graphics, (int) (tileX * Tile.TILE_WIDTH - handler.getCamera().getxOffset()),
                        (int) (tileY * Tile.TILE_HEIGHT - handler.getCamera().getyOffset()));
            }
        }
        entityManager.render(graphics);
    }

    public Tile getTile(int tileX, int tileY) {
        if (tileX < 0 || tileY < 0 || tileX >= width || tileY >= height) {
            return Tile.getTile(0);
        }

        Tile t = Tile.getTile(worldTiles[tileX][tileY]);
        if (t == null) {
            return Tile.getTile(0);
        }

        return t;
    }

    private void loadWorld(String path) {
        BufferedImage mapImage = ImageLoader.loadImage(path);
        height = mapImage.getHeight();
        width = mapImage.getWidth();
        worldTiles = new int[width][height];
        for (int tileY = 0; tileY < height; tileY++) {
            for (int tileX = 0; tileX < width; tileX++) {
                decideTile(mapImage, tileY, tileX);
            }
        }

//        entityManager.addEntity(new Bolt(handler, 1500, 1100));
//        entityManager.addEntity(new Mechatronics(handler, 1400, 1100));
//        entityManager.addEntity(new Plates(handler, 1300, 1100));
//
        entityManager.addEntity(new TorsoA(handler, 1400, 1001));
        entityManager.addEntity(new HeadA(handler, 1400, 1100));
        entityManager.addEntity(new LimbsA(handler, 1400, 1200));
//
//        entityManager.addEntity(new TorsoB(handler, 700, 1100));
//        entityManager.addEntity(new HeadB(handler, 700, 1200));
//        entityManager.addEntity(new LimbsB(handler, 700, 1300));
//
//        entityManager.addEntity(new RobotA(handler, 800, 1300));
//        entityManager.addEntity(new RobotB(handler, 800, 1500));
    }

    private void decideTile(BufferedImage mapImage, int tileY, int tileX) {
        Color currentPix = new Color(mapImage.getRGB(tileX, tileY));

        if (currentPix.equals(FLOOR_COLOR_ID)) {
            worldTiles[tileX][tileY] = 0;
        } else if (currentPix.equals(WALL_COLOR_ID)) {
            worldTiles[tileX][tileY] = themeCode * WallAssets.WALL_VARIATIONS_COUNT + checkAdjacentWall(mapImage, tileX, tileY);
        } else if (currentPix.equals(PLAYER_COLOR_ID)) {
            playerSpawnX = tileX;
            playerSpawnY = tileY;
        } else if (currentPix.equals(ROBOTWB_COLOR_ID)) {
            entityManager.addEntity(new RobotWorkBench(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(TORSOWB_COLOR_ID)) {
            entityManager.addEntity(new TorsoWorkBench(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(HEADWB_COLOR_ID)) {
            entityManager.addEntity(new HeadWorkBench(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(LIMBSWB_COLOR_ID)) {
            entityManager.addEntity(new LimbWorkBench(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(DELIVERY_PODS_COLOR_ID)) {
            entityManager.addEntity(new DeliveryPods(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(BOLT_DISPENSER_COLOR_ID)) {
            entityManager.addEntity(new BoltDispenser(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(PLATE_DISPENSER_COLOR_ID)) {
            entityManager.addEntity(new PlateDispenser(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        } else if (currentPix.equals(MECHATRONICS_DISPENSER_COLOR_ID)) {
            entityManager.addEntity(new MechatronicsDispenser(handler, tileX * Tile.TILE_WIDTH, tileY * Tile.TILE_HEIGHT));
        }
    }

    private int checkAdjacentWall(BufferedImage mapImage, int tileX, int tileY) {
        boolean abovePixel = false, belowPixel = false, leftPixel = false, rightPixel = false;
        if (tileY - 1 >= 0) abovePixel = new Color(mapImage.getRGB(tileX, tileY - 1)).equals(WALL_COLOR_ID);
        if (tileY + 1 < height) belowPixel = new Color(mapImage.getRGB(tileX, tileY + 1)).equals(WALL_COLOR_ID);
        if (tileX - 1 >= 0) leftPixel = new Color(mapImage.getRGB(tileX - 1, tileY)).equals(WALL_COLOR_ID);
        if (tileX + 1 < width) rightPixel = new Color(mapImage.getRGB(tileX + 1, tileY)).equals(WALL_COLOR_ID);

        if (abovePixel && leftPixel && belowPixel && rightPixel) return 30;
        else if (belowPixel && leftPixel && rightPixel) return 27;
        else if (abovePixel && leftPixel && belowPixel) return 29;
        else if (abovePixel && leftPixel && rightPixel) return 26;
        else if (abovePixel && rightPixel && belowPixel) return 28;
        else if (abovePixel && leftPixel) return 4;
        else if (abovePixel && belowPixel) return 17;
        else if (abovePixel && rightPixel) return 3;
        else if (belowPixel && rightPixel) return 1;
        else if (belowPixel && leftPixel) return 2;
        else if (leftPixel && rightPixel) return 16;
        else if (belowPixel) return 19;
        else if (leftPixel) return 21;
        else if (rightPixel) return 20;
        else if (abovePixel) return 18;
        return 0;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
