package spy.overclock.tiles;

import spy.overclock.graphics.Assets.Assets;

import java.awt.image.BufferedImage;

public class FloorTiles extends Tile {

    public static void initialize() throws Exception {
        Tile.setTile(new FloorTiles(Assets.floorTiles, 0));
    }

    public FloorTiles(BufferedImage texture, int id) {
        super(texture, id);
    }

}
