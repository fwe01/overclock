package spy.overclock.tiles;

import spy.overclock.graphics.Assets.Assets;

import java.awt.image.BufferedImage;

public class WallTiles extends Tile {

    public static void initialize() throws Exception {
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_inner_top_left_corner, 1));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_inner_top_right_corner, 2));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_inner_bottom_left_corner, 3));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_inner_bottom_right_corner, 4));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_outer_top_left_corner, 5));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_outer_top_right_corner, 6));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_outer_bottom_left_corner, 7));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_outer_bottom_right_corner, 8));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_down, 9));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_lamp_down, 10));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_accent_down_1, 11));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_accent_down_2, 12));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_left, 13));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_up, 14));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_right, 15));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_double_horizontal, 16));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_plain_double_vertical, 17));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_tip_down, 18));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_tip_up, 19));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_tip_left, 20));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_tip_right, 21));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_void_up, 22));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_void_down, 23));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_void_left, 24));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_void_right, 25));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_double_up, 26));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_double_down, 27));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_double_left, 28));
        Tile.setTile(new WallTiles(Assets.wallAssets.wall_intersection_double_right, 29));

        Tile.setTile(new WallTiles(Assets.wallAssets.wall_cross_section, 30));
    }

    public WallTiles(BufferedImage texture, int id) {
        super(texture, id);
    }

    @Override
    public boolean isSolid() {
        return true;
    }
}
