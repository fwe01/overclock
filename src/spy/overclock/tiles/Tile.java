package spy.overclock.tiles;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
    private static Tile[] tiles = new Tile[256];

    public static final int TILE_WIDTH = 80, TILE_HEIGHT = 80;

    protected BufferedImage texture;
    protected final int id;

    public static void init() {

        try {
            WallTiles.initialize();
            FloorTiles.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setTile(Tile tile) throws Exception {
        if (tiles[tile.id] == null)
            tiles[tile.id] = tile;
        else
            throw new Exception("ID Already Taken");
    }

    public static Tile getTile(int id) {
        return tiles[id];
    }

    public Tile(BufferedImage texture, int id) {
        this.texture = texture;
        this.id = id;
    }

    public void update() {

    }

    public void render(Graphics graphics, int x, int y) {
        graphics.drawImage(texture, x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT, null);
    }

    public boolean isSolid() {
        return false;
    }

    public int getId() {
        return id;
    }
}
