package spy.overclock.events.score;

import spy.overclock.events.score.ScoreAdded.ScoreAdded;

public interface ScoreEventProcessor {
    void onScoreUpdate (ScoreEvent e);
}
