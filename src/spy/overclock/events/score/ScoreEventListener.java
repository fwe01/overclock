package spy.overclock.events.score;

import spy.overclock.events.score.ScoreAdded.ScoreAdded;

import java.util.ArrayList;

public class ScoreEventListener {
    private static ArrayList<ScoreEventProcessor> processors = new ArrayList<ScoreEventProcessor>();

    public static void addEventProcessor (ScoreEventProcessor processor){
        processors.add(processor);
    }

    public static void newScoreEvent (ScoreEvent e){
        for (ScoreEventProcessor processor : processors){
            processor.onScoreUpdate(e);
        }
    }
}
