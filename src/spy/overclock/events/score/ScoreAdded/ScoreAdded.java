package spy.overclock.events.score.ScoreAdded;

import spy.overclock.events.score.ScoreEvent;

public class ScoreAdded implements ScoreEvent {
    private int addScore;

    public ScoreAdded(int addScore) {
        this.addScore = addScore;
    }

    public int getScore() {
        return addScore;
    }
}
