package spy.overclock.events.score.ScoreAdded;

import spy.overclock.events.score.ScoreAdded.ScoreAdded;
import spy.overclock.events.score.ScoreEvent;
import spy.overclock.events.score.ScoreEventListener;
import spy.overclock.events.score.ScoreEventProcessor;
import spy.overclock.events.score.ScoreHandler;

public class ScoreAddedProcessor implements ScoreEventProcessor {
    ScoreHandler scoreHandler;

    public ScoreAddedProcessor(ScoreHandler scoreHandler) {
        this.scoreHandler = scoreHandler;
        ScoreEventListener.addEventProcessor(this);
    }

    public void addScore(int score) {
        scoreHandler.addScore(score);
    }

    @Override
    public void onScoreUpdate(ScoreEvent e) {
        if (e instanceof ScoreAdded)
            addScore(e.getScore());
    }
}
