package spy.overclock.events.score;

import spy.overclock.events.score.ScoreAdded.ScoreAddedProcessor;
import spy.overclock.events.score.ScoreSubtracted.ScoreSubtractedProcessor;

public class ScoreHandler {
    private int score;
    private ScoreAddedProcessor scoreAddedProcessor;
    private ScoreSubtractedProcessor scoreSubtractedProcessor;

    public ScoreHandler() {
        score = 0;

        scoreAddedProcessor = new ScoreAddedProcessor(this);
        scoreSubtractedProcessor = new ScoreSubtractedProcessor(this);
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void subtractScore(int score) {
        this.score -= score;
    }
}
