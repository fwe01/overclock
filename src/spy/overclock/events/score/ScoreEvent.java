package spy.overclock.events.score;

public interface ScoreEvent {
    int getScore();
}
