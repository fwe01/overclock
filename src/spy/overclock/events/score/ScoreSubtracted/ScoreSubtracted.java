package spy.overclock.events.score.ScoreSubtracted;

import spy.overclock.events.score.ScoreEvent;

public class ScoreSubtracted implements ScoreEvent {
    private int reduceScore;

    public ScoreSubtracted(int reduceScore) {
        this.reduceScore = reduceScore;
    }

    public int getScore() {
        return reduceScore;
    }
}
