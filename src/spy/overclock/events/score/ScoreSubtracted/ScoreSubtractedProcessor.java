package spy.overclock.events.score.ScoreSubtracted;

import spy.overclock.events.score.ScoreAdded.ScoreAdded;
import spy.overclock.events.score.ScoreEvent;
import spy.overclock.events.score.ScoreEventListener;
import spy.overclock.events.score.ScoreEventProcessor;
import spy.overclock.events.score.ScoreHandler;

public class ScoreSubtractedProcessor implements ScoreEventProcessor {
    private ScoreHandler scoreHandler;

    public ScoreSubtractedProcessor(ScoreHandler scoreHandler) {
        this.scoreHandler = scoreHandler;
        ScoreEventListener.addEventProcessor(this);
    }

    private void subtractScore(int reduceScore) {
        scoreHandler.subtractScore(reduceScore);
    }

    @Override
    public void onScoreUpdate(ScoreEvent e) {
        if (e instanceof ScoreSubtracted)
            subtractScore(e.getScore());
    }
}
