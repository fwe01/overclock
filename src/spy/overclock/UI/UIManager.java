package spy.overclock.UI;

import spy.overclock.handler.Handler;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class UIManager {
    private Handler handler;
    private ArrayList<UIObject> uiObjects, toBeAddedObjects, toBeRemovedObjects;

    public UIManager(Handler handler) {
        this.handler = handler;
        uiObjects = new ArrayList<UIObject>();
        toBeAddedObjects = new ArrayList<UIObject>();
        toBeRemovedObjects = new ArrayList<UIObject>();
    }

    public void update() {
        uiObjects.addAll(toBeAddedObjects);
        toBeAddedObjects.clear();
        for (UIObject object : uiObjects) {
            object.update();
        }
        uiObjects.removeAll(toBeRemovedObjects);
        toBeRemovedObjects.clear();
    }

    public void render(Graphics graphics) {
        for (UIObject object : uiObjects) {
            object.render(graphics);
        }
    }

    public void onMouseMove(MouseEvent e) {
        for (UIObject object : uiObjects) {
            object.onMouseMove(e);
        }
    }

    public void onMouseRelease(MouseEvent e) {
        for (UIObject object : uiObjects) {
            object.onMouseRelease(e);
        }
    }

    public void addObject(UIObject object) {
        toBeAddedObjects.add(object);
    }

    public void removeObject(UIObject object) {
        toBeRemovedObjects.add(object);
    }
}
