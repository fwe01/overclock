package spy.overclock.UI;

import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.image.BufferedImage;

public class UIImageButton extends UIObject{
    private BufferedImage[] images;
    private ClickListener clicker;
    private boolean active;

    public UIImageButton(float x, float y, int width, int height, BufferedImage[] images, ClickListener clicker) {
        super(x, y, width, height);
        this.images = images;
        this.clicker = clicker;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics graphics) {
        if (hovering || active){
            graphics.drawImage(images[1], (int) x,(int) y, width, height, null);
        } else {
            graphics.drawImage(images[0], (int) x,(int) y, width, height, null);
        }
    }

    @Override
    public void onClick() {
        clicker.onClick();
    }

    public void setActive() {
        active = true;
    }

    public void setDeactive() {
        active = false;
    }
}
