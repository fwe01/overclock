package spy.overclock.UI.InGameUi;

import spy.overclock.UI.UILabel;
import spy.overclock.UI.UIManager;
import spy.overclock.graphics.Assets.Assets;

import java.awt.*;

public class Timer {
    private UILabel timeLabel;

    public Timer(UIManager uiManager) {
        timeLabel = new UILabel("", 1020, 100, Color.WHITE);
        uiManager.addObject(timeLabel);
    }

    public void update() {
    }

    public void setTimer(long time) {
        int second = (int)time / 1000;

        int minute = second / 60;
        second = second % 60;

        timeLabel.setText(String.valueOf(minute) + " : " + (second < 10 ? "0" : "") + String.valueOf(second));
    }

    public void render(Graphics graphics) {
        graphics.drawImage(
                Assets.timeContainer,
                890,
                45,
                128 * 3, 32 * 3, null);
    }
}
