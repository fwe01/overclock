package spy.overclock.UI.InGameUi.Score;

import spy.overclock.UI.UILabel;
import spy.overclock.UI.UIManager;
import spy.overclock.events.score.ScoreHandler;
import spy.overclock.graphics.Assets.Assets;

import java.awt.*;

public class Scoring {
    private UILabel scoreValueLabel;
    private ScoreHandler scoreHandler;

    public Scoring(UIManager uiManager, ScoreHandler scoreHandler) {
        scoreValueLabel = new UILabel("", 120, 100, Color.WHITE);
        uiManager.addObject(scoreValueLabel);
        this.scoreHandler = scoreHandler;
    }

    public void update() {
        scoreValueLabel.setText(String.valueOf(scoreHandler.getScore()));
    }

    public void render(Graphics graphics) {
        graphics.drawImage(
                Assets.moneyContainer,
                5,
                45,
                128 * 3, 32 * 3, null);
    }
}
