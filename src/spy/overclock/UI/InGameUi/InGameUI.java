package spy.overclock.UI.InGameUi;

import spy.overclock.UI.InGameUi.Score.Scoring;
import spy.overclock.UI.UIManager;
import spy.overclock.events.score.ScoreHandler;
import spy.overclock.handler.Handler;

import java.awt.*;

public class InGameUI {
    private Handler handler;
    private UIManager uiManager;
    private Scoring scoring;
    private Timer timer;

    public InGameUI(Handler handler, ScoreHandler scoreHandler) {
        this.handler = handler;

        uiManager = new UIManager(handler);
        scoring = new Scoring(uiManager, scoreHandler);
        timer = new Timer(uiManager);
    }

    public void update() {
        scoring.update();
        uiManager.update();
    }

    public void render(Graphics graphics) {
        scoring.render(graphics);
        timer.render(graphics);
        uiManager.render(graphics);
    }

    public Timer getTimer() {
        return timer;
    }
}
