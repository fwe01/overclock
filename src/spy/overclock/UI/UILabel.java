package spy.overclock.UI;

import java.awt.*;

public class UILabel extends UIObject {
    private String text;
    private static final Font textFont = new Font("Arial", Font.BOLD, 32);
    private Color textColor;

    public UILabel(String text , float x, float y, Color textColor) {
        super(x, y, 0, 0);
        this.text = text;
        this.textColor = textColor;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics graphics) {
        graphics.setColor(textColor);
        graphics.setFont(textFont);
        graphics.drawString(text, (int) x, (int) y);
    }

    @Override
    public void onClick() {

    }
}
