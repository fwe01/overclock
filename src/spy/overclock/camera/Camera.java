package spy.overclock.camera;

import spy.overclock.Game;
import spy.overclock.entity.Entity;
import spy.overclock.handler.Handler;

public class Camera {
    private float xOffset, yOffset;
    private Handler handler;

    public Camera(Handler handler, float xOffset, float yOffset) {
        this.handler = handler;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    public void centerOnEntity(Entity e) {
        xOffset = e.getX() - handler.getWidth() / 2 + e.getWidth() / 2;
        yOffset = e.getY() - handler.getHeight() / 2 + e.getHeight() / 2;
    }

    public void moveCamera(float xMove, float yMove) {
        xOffset += xMove;
        yOffset += yMove;
    }

    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }
}
