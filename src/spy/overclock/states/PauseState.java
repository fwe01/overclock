package spy.overclock.states;

import spy.overclock.UI.ClickListener;
import spy.overclock.UI.UIImageButton;
import spy.overclock.UI.UILabel;
import spy.overclock.UI.UIManager;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class PauseState extends State {
    private UIManager uiManager;
    private boolean escapeWasPressed;

    public PauseState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);

        //add play button
        uiManager.addObject(new UIImageButton(350, 450, 128, 128, Assets.buttonAssets.playButton,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getPauseState()) {
                            StateManager.play();
                        }
                    }
                }
        ));

        //add menu button
        uiManager.addObject(new UIImageButton(750, 450, 128, 128, Assets.buttonAssets.menuButton,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getPauseState()) {
                            StateManager.setState(StateManager.getMenuState());
                        }
                    }
                }
        ));

        //add label
        uiManager.addObject(new UILabel("Continue", 335, 450, Color.WHITE));

        uiManager.addObject(new UILabel("Main Menu", 720, 450, Color.WHITE));
    }

    @Override
    public void update() {
        handler.getMouseManager().setUiManager(uiManager);
        uiManager.update();

        boolean isPause = handler.getKeyManager().pause;
        if (isPause && !escapeWasPressed) {
            StateManager.play();
        }

        if (isPause) {
            escapeWasPressed = true;
        } else {
            escapeWasPressed = false;
        }
    }

    @Override
    public void render(Graphics g) {
        uiManager.render(g);
    }
}
