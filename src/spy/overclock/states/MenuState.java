package spy.overclock.states;

import spy.overclock.UI.ClickListener;
import spy.overclock.UI.UIImageButton;
import spy.overclock.UI.UIManager;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class MenuState extends State {
    private UIManager uiManager;
    private UIImageButton playButton, level1Button, level2Button, level3Button;

    public MenuState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);

        //add play button
        uiManager.addObject(playButton = new UIImageButton(550, 300, 128, 128, Assets.buttonAssets.playButton,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getMenuState()){
                            System.out.println("JALAN GAME");
                            StateManager.play();
                        }
                    }
                }
        ));

        //level one button
        uiManager.addObject(level1Button = new UIImageButton(350, 450, 128, 128, Assets.buttonAssets.level1Button,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getMenuState()){
                            System.out.println("JALAN GAME 1");
                            level1Button.setActive();
                            level2Button.setDeactive();
                            level3Button.setDeactive();
                            StateManager.setSelectedLevel(1);
                        }
                    }
                }
        ));

        //level two button
        uiManager.addObject(level2Button = new UIImageButton(550, 450, 128, 128, Assets.buttonAssets.level2Button,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getMenuState()){
                            System.out.println("JALAN GAME 2");
                            level1Button.setDeactive();
                            level2Button.setActive();
                            level3Button.setDeactive();
                            StateManager.setSelectedLevel(2);
                        }
                    }
                }
        ));

        //level three button
        uiManager.addObject(level3Button = new UIImageButton(750, 450, 128, 128, Assets.buttonAssets.level3Button,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getMenuState()){
                            System.out.println("JALAN GAME 3");
                            level1Button.setDeactive();
                            level2Button.setDeactive();
                            level3Button.setActive();
                            StateManager.setSelectedLevel(3);
                        }
                    }
                }
        ));

        //initial button state
        level1Button.setActive();
        StateManager.setSelectedLevel(1);
    }

    @Override
    public void update() {
        handler.getMouseManager().setUiManager(uiManager);
        uiManager.update();
    }

    @Override
    public void render(Graphics graphics) {
        uiManager.render(graphics);

        //draw title
        graphics.drawImage(Assets.title,
                350,
                200,
                Assets.title.getWidth() * 3, Assets.title.getHeight() * 3, null);
    }
}
