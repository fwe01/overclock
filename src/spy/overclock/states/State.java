package spy.overclock.states;

import spy.overclock.Game;
import spy.overclock.handler.Handler;

import java.awt.*;

public abstract class State {

    protected Handler handler;

    public State(Handler handler){
        this.handler = handler;
    }

    public abstract void update();
    public abstract void render(Graphics g);

}
