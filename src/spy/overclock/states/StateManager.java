package spy.overclock.states;

import spy.overclock.handler.Handler;

import java.awt.*;

public class StateManager {
    private static State currentState = null, pauseState, endGameState, menuState, selectedLevel;
    private static Handler handler;

    public static void init(Handler handler) {
        StateManager.handler = handler;
        pauseState = new PauseState(handler);
        menuState = new MenuState(handler);
        endGameState = new EndGameState(handler);
        currentState = menuState;
    }

    public static void update() {
        handler.getKeyManager().update();

        if (currentState == null) return;

        currentState.update();
    }

    public static void render(Graphics graphics) {
        if (currentState == null) return;

        currentState.render(graphics);
    }

    public static void setState(State state) {
        currentState = state;
    }

    public static void setSelectedLevel(int levelCode) {
        switch (levelCode) {
            case 1:
                selectedLevel  = new GameState(StateManager.handler, "/World/map1.png");
                break;
            case 2:
                selectedLevel  = new GameState(StateManager.handler, "/World/map2.png");
                break;
            case 3:
                selectedLevel  = new GameState(StateManager.handler, "/World/map3.png");
                break;
        }
    }

    public static void play (){
        handler.setWorld(((GameState)selectedLevel).getWorld());
        currentState = selectedLevel;
    }

    public static State getState() {
        return currentState;
    }

    public static State getPauseState() {
        return pauseState;
    }

    public static State getMenuState() {
        return menuState;
    }

    public static State getEndGameState() {
        return endGameState;
    }
}

