package spy.overclock.states;

import spy.overclock.UI.InGameUi.InGameUI;
import spy.overclock.events.score.ScoreHandler;
import spy.overclock.handler.Handler;
import spy.overclock.world.World;

import java.awt.*;

public class GameState extends State {
    private World world;
    private InGameUI inGameUI;
    private ScoreHandler scoreHandler;
    private long time;
    private long lastTime;

    private boolean escapeWasPressed;

    public GameState(Handler handler, String path) {
        super(handler);
        world = new World(handler, path, 0);
        scoreHandler = new ScoreHandler();
        inGameUI = new InGameUI(handler, scoreHandler);

        time = 120 * 1000; //->second to ms;
        lastTime = System.currentTimeMillis();
    }

    @Override
    public void update() {
        world.update();
        inGameUI.update();
        long now = System.currentTimeMillis();

        long deltaTime = now - lastTime;
        time = time - deltaTime;
        lastTime = now;
        inGameUI.getTimer().setTimer(time);

        boolean isPause = handler.getKeyManager().pause;
        if (isPause && !escapeWasPressed) {
            StateManager.setState(StateManager.getPauseState());
        }

        if (isPause) {
            escapeWasPressed = true;
        } else {
            escapeWasPressed = false;
        }

        if (time <= 0) {
            EndGameState endGameState = (EndGameState) StateManager.getEndGameState();
            endGameState.setScoreHandler(this.scoreHandler);
            StateManager.setState(endGameState);
        }
    }

    @Override
    public void render(Graphics graphics) {
        world.render(graphics);
        inGameUI.render(graphics);
    }

    public World getWorld() {
        return world;
    }

    public ScoreHandler getScoreHandler() {
        return scoreHandler;
    }
}
