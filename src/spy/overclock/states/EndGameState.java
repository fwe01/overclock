package spy.overclock.states;

import spy.overclock.UI.ClickListener;
import spy.overclock.UI.UIImageButton;
import spy.overclock.UI.UILabel;
import spy.overclock.UI.UIManager;
import spy.overclock.events.score.ScoreHandler;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;

import java.awt.*;

public class EndGameState extends State {
    private UIManager uiManager;
    private UILabel uiLabel;
    private UIImageButton menuButton;
    private ScoreHandler scoreHandler;

    public EndGameState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        uiManager.addObject(uiLabel = new UILabel("score", 575, 250, Color.RED));

        uiManager.addObject(menuButton = new UIImageButton(550, 450, 128, 128, Assets.buttonAssets.menuButton,
                new ClickListener() {
                    @Override
                    public void onClick() {
                        if (StateManager.getState() == StateManager.getEndGameState()) {
                            StateManager.setState(StateManager.getMenuState());
                        }
                    }
                }
        ));
    }

    @Override
    public void update() {
        handler.getMouseManager().setUiManager(uiManager);
        if (scoreHandler != null) {
            uiLabel.setText(String.valueOf(scoreHandler.getScore()));
        }
        uiManager.update();
    }

    @Override
    public void render(Graphics g) {
        uiManager.render(g);
    }

    public void setScoreHandler(ScoreHandler scoreHandler) {
        this.scoreHandler = scoreHandler;
    }
}
