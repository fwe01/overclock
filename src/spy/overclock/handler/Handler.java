package spy.overclock.handler;

import spy.overclock.Game;
import spy.overclock.camera.Camera;
import spy.overclock.input.KeyManager;
import spy.overclock.input.MouseManager;
import spy.overclock.window.Window;
import spy.overclock.world.World;

public class Handler {
    private Game game;
    private World world;

    public Handler(Game game) {
        this.game = game;
    }

    public Camera getCamera() {
        return game.getCamera();
    }

    public KeyManager getKeyManager() {
        return game.getKeyManager();
    }

    public MouseManager getMouseManager(){
        return game.getMouseManager();
    }

    public int getWidth (){
        return game.getWidth();
    }

    public int getHeight(){
        return game.getHeight();
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public boolean getRenderCollisionBox (){
        return game.getRenderCollisionBox();
    }

    public Window getGameWindow() {
        return game.getGameWindow();
    }

}
