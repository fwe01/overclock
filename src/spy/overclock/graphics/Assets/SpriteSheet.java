package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class SpriteSheet {
    private BufferedImage sheets;

    public SpriteSheet(BufferedImage sheets) {
        this.sheets = sheets;
    }

    public BufferedImage crop(int x, int y, int width, int height) {
        return sheets.getSubimage(x * width, y * height, width, height);
    }

    public BufferedImage crop(int x, int y) {
        return sheets.getSubimage(x * 32, y * 32, 32, 32);
    }
}
