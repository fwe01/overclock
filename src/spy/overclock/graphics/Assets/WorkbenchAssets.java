package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class WorkbenchAssets {
    public BufferedImage workbench_limbs_hands;
    public BufferedImage workbench_limbs_legs;
    public BufferedImage workbench_torso;
    public BufferedImage workbench_head;
    public BufferedImage workbench_robot;

    public WorkbenchAssets() {
        SpriteSheet work_bench_sheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Workbench/workbench.png"));

        workbench_head = work_bench_sheet.crop(0, 0);
        workbench_torso = work_bench_sheet.crop(1, 0);
        workbench_limbs_hands = work_bench_sheet.crop(2, 0);
        workbench_limbs_legs = work_bench_sheet.crop(3, 0);

        workbench_robot = (new SpriteSheet(ImageLoader.loadImage("/Sprites/Workbench/robo_workbench.png"))).crop(0, 0, 96, 96);
    }
}
