package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class RobotAssets {
    public BufferedImage robotA, robotB;

    public RobotAssets() {
        SpriteSheet robot_sheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Robots/robot_sheet.png"));
        robotA = robot_sheet.crop(0, 0, 48, 48);
        robotB = robot_sheet.crop(1, 0, 48, 48);
    }
}
