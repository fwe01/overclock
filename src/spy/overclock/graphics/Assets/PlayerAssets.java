package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class PlayerAssets {
    private SpriteSheet playerSheet;
    public BufferedImage[]
            player_down,
            player_idle_down,
            player_left,
            player_idle_left,
            player_up,
            player_idle_up,
            player_right,
            player_idle_right;

    public BufferedImage[]
            player_pickup_down,
            player_pickup_idle_down,
            player_pickup_left,
            player_pickup_idle_left,
            player_pickup_up,
            player_pickup_idle_up,
            player_pickup_right,
            player_pickup_idle_right;

    public PlayerAssets() {
        playerSheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Player/player.png"));

        //walking anim
        Assets.loadAndCropSheet(player_down = new BufferedImage[4], playerSheet, 0);
        Assets.loadAndCropSheet(player_idle_down = new BufferedImage[4], playerSheet, 1);

        Assets.loadAndCropSheet(player_left = new BufferedImage[4], playerSheet, 2);
        Assets.loadAndCropSheet(player_idle_left = new BufferedImage[4], playerSheet, 3);

        Assets.loadAndCropSheet(player_up = new BufferedImage[4], playerSheet, 4);
        Assets.loadAndCropSheet(player_idle_up = new BufferedImage[4], playerSheet, 5);

        Assets.loadAndCropSheet(player_right = new BufferedImage[4], playerSheet, 6);
        Assets.loadAndCropSheet(player_idle_right = new BufferedImage[4], playerSheet, 7);

        //pickuping anim
        Assets.loadAndCropSheet(player_pickup_down = new BufferedImage[4], playerSheet, 8);
        Assets.loadAndCropSheet(player_pickup_idle_down = new BufferedImage[4], playerSheet, 9);

        Assets.loadAndCropSheet(player_pickup_left = new BufferedImage[4], playerSheet, 10);
        Assets.loadAndCropSheet(player_pickup_idle_left = new BufferedImage[4], playerSheet, 11);

        Assets.loadAndCropSheet(player_pickup_up = new BufferedImage[4], playerSheet, 12);
        Assets.loadAndCropSheet(player_pickup_idle_up = new BufferedImage[4], playerSheet, 13);

        Assets.loadAndCropSheet(player_pickup_right = new BufferedImage[4], playerSheet, 14);
        Assets.loadAndCropSheet(player_pickup_idle_right = new BufferedImage[4], playerSheet, 15);
    }
}
