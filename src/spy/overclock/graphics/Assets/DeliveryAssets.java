package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class DeliveryAssets {
    private SpriteSheet deliveryPodSpriteSheet;
    public BufferedImage[] doorOpen, doorClose, outgoingRobots, incomingPod, complete_send;

    public DeliveryAssets() {
        deliveryPodSpriteSheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Deliveries/delivery pods.png"));
        Assets.loadAndCropSheet(doorOpen = new BufferedImage[6], deliveryPodSpriteSheet, 0, 80, 80);
        Assets.loadAndCropSheet(doorClose = new BufferedImage[6], deliveryPodSpriteSheet, 1, 80, 80);
        Assets.loadAndCropSheet(outgoingRobots = new BufferedImage[17], deliveryPodSpriteSheet, 2, 80, 80);
        Assets.loadAndCropSheet(incomingPod = new BufferedImage[16], deliveryPodSpriteSheet, 3, 80, 80);

        complete_send = new BufferedImage[39];

        System.arraycopy(doorClose, 0, complete_send, 0, 6);
        System.arraycopy(outgoingRobots, 0, complete_send, 6, 17);
        System.arraycopy(incomingPod, 0, complete_send, 23, 16);
    }
}
