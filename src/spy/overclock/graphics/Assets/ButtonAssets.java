package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class ButtonAssets {
    private SpriteSheet buttonSheet;
    public BufferedImage[] playButton, menuButton, optionButton, level1Button, level2Button, level3Button;

    public ButtonAssets() {
        buttonSheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Button/playButton.png"));

        Assets.loadAndCropSheet(playButton = new BufferedImage[2], buttonSheet, 0);
        Assets.loadAndCropSheet(menuButton = new BufferedImage[2], buttonSheet, 1);
        Assets.loadAndCropSheet(optionButton = new BufferedImage[2], buttonSheet,2);
        Assets.loadAndCropSheet(level1Button = new BufferedImage[2], buttonSheet,3);
        Assets.loadAndCropSheet(level2Button = new BufferedImage[2], buttonSheet,4);
        Assets.loadAndCropSheet(level3Button = new BufferedImage[2], buttonSheet,5);
    }
}
