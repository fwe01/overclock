package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class DispensersAssets {
    private SpriteSheet dispenserSheet;
    public BufferedImage[] incomingScrew, incomingSheet, incomingMechatronics, outgoing;

    public DispensersAssets() {
        dispenserSheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Dispenser/dispenser.png"));
        Assets.loadAndCropSheet(incomingScrew = new BufferedImage[16], dispenserSheet, 0, 64, 64);
        Assets.loadAndCropSheet(incomingSheet = new BufferedImage[16], dispenserSheet, 1, 64, 64);
        Assets.loadAndCropSheet(incomingMechatronics = new BufferedImage[16], dispenserSheet, 2, 64, 64);
    }
}
