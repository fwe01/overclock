package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class WallAssets {

    public static final int WALL_VARIATIONS_COUNT = 30;

    //wall
    public BufferedImage
            wall_inner_top_left_corner,
            wall_inner_top_right_corner,
            wall_inner_bottom_left_corner,
            wall_inner_bottom_right_corner,

    wall_outer_top_left_corner,
            wall_outer_top_right_corner,
            wall_outer_bottom_left_corner,
            wall_outer_bottom_right_corner,

    wall_plain_down,
            wall_lamp_down,
            wall_accent_down_1,
            wall_accent_down_2,

    wall_plain_left,
            wall_plain_up,
            wall_plain_right,

    wall_plain_double_horizontal,
            wall_plain_double_vertical,

    wall_tip_down,
            wall_tip_up,
            wall_tip_left,
            wall_tip_right,

    wall_intersection_void_up,
            wall_intersection_void_down,
            wall_intersection_void_left,
            wall_intersection_void_right,

    wall_intersection_double_up,
            wall_intersection_double_down,
            wall_intersection_double_left,
            wall_intersection_double_right,

    wall_cross_section;

    public WallAssets() {
        SpriteSheet wallSheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Walls/walls_sheets.png"));

        wall_inner_top_left_corner = wallSheet.crop(0, 0);
        wall_inner_top_right_corner = wallSheet.crop(2, 0);
        wall_inner_bottom_left_corner = wallSheet.crop(0, 2);
        wall_inner_bottom_right_corner = wallSheet.crop(2, 2);

        wall_outer_top_left_corner = wallSheet.crop(0, 3);
        wall_outer_top_right_corner = wallSheet.crop(1, 3);
        wall_outer_bottom_left_corner = wallSheet.crop(0, 4);
        wall_outer_bottom_right_corner = wallSheet.crop(1, 4);

        wall_plain_down = wallSheet.crop(1, 0);
        wall_plain_left = wallSheet.crop(0, 1);
        wall_plain_right = wallSheet.crop(2, 1);
        wall_plain_up = wallSheet.crop(1, 2);

        wall_lamp_down = wallSheet.crop(4, 0);
        wall_accent_down_1 = wallSheet.crop(3, 0);
        wall_accent_down_2 = wallSheet.crop(5, 0);

        wall_plain_double_horizontal = wallSheet.crop(6, 2);
        wall_plain_double_vertical = wallSheet.crop(3, 4);

        wall_tip_down = wallSheet.crop(4, 5);
        wall_tip_up = wallSheet.crop(4, 3);
        wall_tip_left = wallSheet.crop(5, 2);
        wall_tip_right = wallSheet.crop(7, 2);

        wall_intersection_void_down = wallSheet.crop(2, 3);
        wall_intersection_void_up = wallSheet.crop(2, 5);
        wall_intersection_void_left = wallSheet.crop(4, 2);
        wall_intersection_void_right = wallSheet.crop(3, 2);

        wall_intersection_double_down = wallSheet.crop(3, 3);
        wall_intersection_double_up = wallSheet.crop(3, 5);
        wall_intersection_double_left = wallSheet.crop(3, 1);
        wall_intersection_double_right = wallSheet.crop(4, 1);

        wall_cross_section = wallSheet.crop(5, 1);

    }
}
