package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class ResourceAssets {
    public BufferedImage bolt, plates, mechatronics;

    public ResourceAssets() {
        bolt = ImageLoader.loadImage("/Sprites/Resources/bolt.png");
        plates = ImageLoader.loadImage("/Sprites/Resources/plates.png");
        mechatronics = ImageLoader.loadImage("/Sprites/Resources/mechatronics.png");
    }
}
