package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class PartAssets {
    public BufferedImage torsoA, limbsA, headA, brokenPartA;
    public BufferedImage torsoB, limbsB, headB, brokenPartB;

    public PartAssets() {
        SpriteSheet work_bench_sheet = new SpriteSheet(ImageLoader.loadImage("/Sprites/Parts/part_sheet.png"));

        headA = work_bench_sheet.crop(0, 0);
        torsoA = work_bench_sheet.crop(1, 0);
        limbsA = work_bench_sheet.crop(2, 0);
        brokenPartA = work_bench_sheet.crop(3, 0);

        headB = work_bench_sheet.crop(0, 1);
        torsoB = work_bench_sheet.crop(1, 1);
        limbsB = work_bench_sheet.crop(2, 1);
        brokenPartB = work_bench_sheet.crop(3, 1);
    }
}
