package spy.overclock.graphics.Assets;

import java.awt.image.BufferedImage;

public class Assets {
    public static WallAssets wallAssets;
    public static PartAssets partAssets;
    public static RobotAssets robotAssets;
    public static BufferedImage floorTiles;
    public static ButtonAssets buttonAssets;
    public static PlayerAssets playerAssets;
    public static DeliveryAssets deliveryAssets;
    public static ResourceAssets resourceAssets;
    public static WorkbenchAssets workbenchAssets;
    public static DispensersAssets dispensersAssets;

    public static BufferedImage title;
    public static BufferedImage moneyContainer;
    public static BufferedImage timeContainer;

    public static void init() {
        floorTiles = ImageLoader.loadImage("/Sprites/Tiles/tiles.png");
        title = ImageLoader.loadImage("/Sprites/Title/GameTitle.png");
        moneyContainer = ImageLoader.loadImage("/Sprites/UI/money_Container.png");
        timeContainer = ImageLoader.loadImage("/Sprites/UI/time_container.png");

        wallAssets = new WallAssets();
        partAssets = new PartAssets();
        robotAssets = new RobotAssets();
        playerAssets = new PlayerAssets();
        buttonAssets = new ButtonAssets();
        deliveryAssets = new DeliveryAssets();
        resourceAssets = new ResourceAssets();
        workbenchAssets = new WorkbenchAssets();
        dispensersAssets = new DispensersAssets();
    }

    public static void loadAndCropSheet(BufferedImage[] animArray, SpriteSheet spriteSheet, int y) {
        for (int i = 0; i < animArray.length; i++)
            animArray[i] = spriteSheet.crop(i, y);
    }

    public static void loadAndCropSheet(BufferedImage[] animArray, SpriteSheet spriteSheet, int y, int width, int height) {
        for (int i = 0; i < animArray.length; i++)
            animArray[i] = spriteSheet.crop(i, y, width, height);
    }
}
