package spy.overclock;

import java.awt.*;
import java.awt.image.BufferStrategy;

import spy.overclock.camera.Camera;
import spy.overclock.graphics.Assets.Assets;
import spy.overclock.handler.Handler;
import spy.overclock.input.KeyManager;
import spy.overclock.input.MouseManager;
import spy.overclock.states.StateManager;
import spy.overclock.tiles.Tile;
import spy.overclock.utils.FPSCounter;
import spy.overclock.window.Window;
import spy.overclock.world.World;

public class Game implements Runnable {
    private Window gameWindow;
    private int width, height;
    private Boolean running = false,
            renderCollisionBox = false;
    private Thread thread;
    private String windowTitle;
    private BufferStrategy bStrategy;
    private Graphics graphics;
    private KeyManager keyManager;
    private MouseManager mouseManager;
    private Camera camera;
    private Handler handler;

    public Game(String windowTitle, int width, int height) {
        this.width = width;
        this.height = height;
        this.windowTitle = windowTitle;
        keyManager = new KeyManager();
        mouseManager = new MouseManager();
    }

    public Camera getCamera() {
        return camera;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public MouseManager getMouseManager() {
        return mouseManager;
    }

    public Boolean getRenderCollisionBox() {
        return renderCollisionBox;
    }

    public Window getGameWindow() {
        return gameWindow;
    }

    private void init() {
        gameWindow = new Window(windowTitle, this.width, this.height);
        gameWindow.getFrame().addKeyListener(keyManager);
        gameWindow.getFrame().addMouseListener(mouseManager);
        gameWindow.getFrame().addMouseMotionListener(mouseManager);
        gameWindow.getCanvas().addMouseListener(mouseManager);
        gameWindow.getCanvas().addMouseMotionListener(mouseManager);

        Assets.init();
        Tile.init();

        handler = new Handler(this);
        camera = new Camera(this.handler, 0, 0);
        StateManager.init(this.handler);

        renderCollisionBox = true;
    }

    private void update() {
        StateManager.update();
    }

    private void render() {
        bStrategy = gameWindow.getCanvas().getBufferStrategy();
        if (bStrategy == null) {
            gameWindow.getCanvas().createBufferStrategy(3);
            return;
        }
        graphics = bStrategy.getDrawGraphics();

        //clear screen
        graphics.setColor(World.WORLD_BACKGROUND);
        graphics.fillRect(0, 0, width, height);

        //Draw objects
        StateManager.render(graphics);

        bStrategy.show();
        graphics.dispose();
    }

    @Override
    public void run() {
        init();

        while (running) {
            FPSCounter.countTime();

            if (FPSCounter.delta >= 1) {
                FPSCounter.updateCounter();
                update();
                render();
            }

            FPSCounter.countFPS();
        }

        stop();
    }

    public synchronized void start() {
        if (running) {
            return;
        }
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if (!running) {
            return;
        }
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
